
$(document).ready(function(){
let currentDate = new Date;
let date = currentDate.toDateString();
let dayOfTheWeek;
let fullDate;


setDayOfTheWeek();
setFullDate();


$('#calendar_btn').click(function (){
$('#datepicker').datepicker()
    .on('changeDate', changeDate)
});

// $('#plus_btn').click(function (){})


function setDayOfTheWeek(){
    dayOfTheWeek = date.slice(0, 3);
    dayOfTheWeek = (dayOfTheWeek === 'Sun') ? 'Sunday' :
            (dayOfTheWeek === 'Mon') ? 'Monday' :
                (dayOfTheWeek === 'Tue') ? 'Tuesday' :
                    (dayOfTheWeek === 'Wed') ? 'Wednesday' :
                        (dayOfTheWeek === 'Thu') ? 'Thursday' :
                            (dayOfTheWeek === 'Fri') ? 'Friday' : 'Saturday';
    $('#day-of-the-week').text(dayOfTheWeek);
}

function setFullDate() {
    fullDate = date.slice(4, 16);
    $('#date').text(fullDate);
}

function changeDate() {
    date = $('#datepicker').datepicker('getDate').toDateString();
    setDayOfTheWeek();
    setFullDate();
}
});

